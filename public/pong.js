const canvas = document.getElementById("pong");
const ctx = canvas.getContext('2d');

const MAX_POINTS = 5;

let hit = new Audio();
let wall = new Audio();
let userScore = new Audio();
let comScore = new Audio();

hit.src = "sounds/hit.mp3";
wall.src = "sounds/wall.mp3";
comScore.src = "sounds/comScore.mp3";
userScore.src = "sounds/userScore.mp3";

// Ball object
const ball = {
    x : canvas.width/2,
    y : canvas.height/2,
    radius : 20,
    velocityX : 5,
    velocityY : 5,
    speed : 4,
    color : "#F9F7D8"
}

// Player Paddle
const user = {
    x : 0, // left side of canvas
    y : (canvas.height - 100)/2, // -100 the height of paddle
    width : 20,
    height : 100,
    score : 0,
    color : "#1375D0"
}

// Computer Paddle
const computer = {
    x : canvas.width - 20, // - width of paddle
    y : (canvas.height - 100)/2, // -100 the height of paddle
    width : 20,
    height : 100,
    score : 0,
    color : "#D0133E"
}

// net
const net = {
    x : (canvas.width - 2)/2,
    y : 0,
    height : 10,
    width : 2,
    color : "WHITE"
}

function drawRect(x, y, w, h, color){
    ctx.fillStyle = color;
    ctx.fillRect(x, y, w, h);
}

function drawArc(x, y, r, color){
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.arc(x,y,r,0,Math.PI*2,true);
    ctx.closePath();
    ctx.fill();
}

canvas.addEventListener("mousemove", getMousePos);

function getMousePos(evt){
    let rect = canvas.getBoundingClientRect();
    user.y = evt.clientY - rect.top - user.height/2;
}

document.onkeydown = function (e) {
    switch (e.key) {
        case 'ArrowUp':
        if((user.y - user.height/4) > (user.height/2 * -1))
            user.y = user.y - user.height/4;
            break;
        case 'ArrowDown':
            if((user.y + user.height/2) < canvas.height)
                user.y = user.y + user.height/4;
            break;
    }
};

function resetBall(){
    ball.x = canvas.width/2;
    ball.y = canvas.height/2;
    ball.velocityX = -ball.velocityX;
    ball.speed = 4;
}

function drawNet(){
    for(let i = 0; i <= canvas.height; i+=15){
        drawRect(net.x, net.y + i, net.width, net.height, net.color);
    }
}

function drawText(text,x,y){
    ctx.fillStyle = "#FFF";
    ctx.font = "75px fantasy";
    ctx.fillText(text, x, y);
}

function drawTextColor(text,x,y, color){
    ctx.fillStyle = color;
    ctx.font = "55px fantasy";
    ctx.fillText(text, x, y);
}

// collision detection
function collision(ball,player){
    player.top = player.y;
    player.bottom = player.y + player.height;
    player.left = player.x;
    player.right = player.x + player.width;
    
    ball.top = ball.y - ball.radius;
    ball.bottom = ball.y + ball.radius;
    ball.left = ball.x - ball.radius;
    ball.right = ball.x + ball.radius;
    
    return player.left < ball.right && player.top < ball.bottom && player.right > ball.left && player.bottom > ball.top;
}

function update(){

    //scoring system
    if( ball.x - ball.radius < 0 ){
        computer.score++;
        comScore.play();
        resetBall();
    }else if( ball.x + ball.radius > canvas.width){
        user.score++;
        userScore.play();
        resetBall();
    }
    
    ball.x += ball.velocityX;
    ball.y += ball.velocityY;
    
    // simple AI
    computer.y += ((ball.y - (computer.y + computer.height/2)))*0.02;
    
    // when the ball collides with bottom and top walls we inverse the y velocity.
    if(ball.y - ball.radius < 0 || ball.y + ball.radius > canvas.height){
        ball.velocityY = -ball.velocityY;
        wall.play();
    }
    
    let player = (ball.x + ball.radius < canvas.width/2) ? user : computer;
    
    if(collision(ball,player)){
        hit.play();
        let collidePoint = (ball.y - (player.y + player.height/2));
        collidePoint = collidePoint / (player.height/2);
        
        let angleRad = (Math.PI/4) * collidePoint;
        
        let direction = (ball.x + ball.radius < canvas.width/2) ? 1 : -1;
        ball.velocityX = direction * ball.speed * Math.cos(angleRad);
        ball.velocityY = ball.speed * Math.sin(angleRad);

        ball.speed += 0.1;
    }
}

function render(){
    drawRect(0, 0, canvas.width, canvas.height, "#000");
    
    drawText(user.score,canvas.width/4,canvas.height/5);
    drawText(computer.score,3*canvas.width/4,canvas.height/5);
    drawNet();

    drawRect(user.x, user.y, user.width, user.height, user.color);
    drawRect(computer.x, computer.y, computer.width, computer.height, computer.color);
    drawArc(ball.x, ball.y, ball.radius, ball.color);
}

function gameOver(){
    drawText(user.score,canvas.width/4,canvas.height/5);
    if(user.score === MAX_POINTS)
    {
        drawTextColor("Player wins!", canvas.width/3, canvas.height/4, "BLUE");
    }
    else{
        drawTextColor("Computer wins.", canvas.width/3, canvas.height/4, "RED");
    }
}

function game(){
    if(user.score < MAX_POINTS && computer.score < MAX_POINTS){
        update();
        render();
    }
    else{
        gameOver();
    }
}

let framePerSecond = 80;
let loop = setInterval(game,1000/framePerSecond);